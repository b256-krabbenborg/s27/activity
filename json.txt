{
  "users": [
    {
      "first_name": "Shiela Mae",
      "last_name": "Krabbenborg",
      "email": "agulan.shielamae@gmail.com",
      "password": "password123",
      "is_admin": false,
      "mobile_number": "+31627890391"
    }
  ],
  "orders": [
    {
      "order_id": 1,
      "user_id": 1,
      "transaction_date": "2022-03-18",
      "status": "shipped",
      "total": 100.00
    }
  ],
  "products": [
    {
      "product_id": 1,
      "name": "iPhone 13",
      "description": "Apple's latest smartphone",
      "price": 999.99,
      "stocks": 50,
      "is_active": true,
      "sku": "IPH13-BLK-64GB"
    }
  ],
  "order_products": [
    {
      "order_id": 1,
      "product_id": 1,
      "quantity": 2,
      "price": 999.99,
      "sub_total": 1999.98
    }
  ]
}
